INTRODUCTION
------------

This module gives you extra settings to control the behavior of node's
links that are generated in teaser mode. Basically it concerns "Read more"
links. It gives the abaility to :

 * Move node links into an existing field group (if Field Group module is installed)
 * Define node links weight
 * Hide node links
 * Add custom CSS class(es)
 * Customize the text of the "Read more" link

These settings are content type specifics. Each content type can have its own settings.


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

 * If the Field Group module (https://www.drupal.org/project/field_group) is installed,
   this module propose an extra setting to move node links into an
   existing field group.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Visit admin/config/content/node_links_options to set custom options

 * If you want anyone besides the administrative user to be able
   to configure actions (usually a bad idea), they must be given
   the "administer actions" access permission:


MAINTAINERS
-----------

Current maintainers:

 * Geoffray Warnants (https://www.drupal.org/user/3044263/)