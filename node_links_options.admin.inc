<?php

function node_links_options_admin_form($form, &$form_state)
{
	$settings = variable_get('node_links_options_variables', array());

    $form['node_links_options_variables'] = array(
	    '#type' => 'vertical_tabs',
	    '#tree' => true,
  	);

    foreach (node_type_get_types() as $type => $node_type) {

	  	$form['node_links_options_variables'][$type] = array(
			'#type' => 'fieldset',
			'#title' => $node_type->name,
	    );

    	$form['node_links_options_variables'][$type][] = array(
			'#markup' => '<h3>'.t('Node type: @type', array('@type' => $node_type->name)).'</h3>',
	    );

	    $form['node_links_options_variables'][$type]['mode'] = array(
			'#type' => 'select',
			'#title' => t('View mode'),
			'#default_value' => 'teaser',
			'#options' => array(
				'teaser' => t('Teaser')
			),
			'#disabled' => true,
			'#description' => t('This module only works with the teaser view mode.'),
	    );

	    $form['node_links_options_variables'][$type]['field_group'] = array(
	        '#type' => 'select',
	        '#title' => t('Move links to field group'),
	        '#default_value' => isset($settings[$type]['field_group']) ? $settings[$type]['field_group'] : '',
	        '#options' => array(),
	        '#empty_value' => '',
	        '#empty_option' => t('None'),
	    );

	    if (module_exists('field_group')) {
		    $groups = field_group_read_groups(array(
	            'entity_type' => 'node',
	            'bundle'      => $type,
	            'mode'   => 'teaser',
        	));

	    	if (isset($groups['node'][$type]['teaser'])) {
	    		$form['node_links_options_variables'][$type]['field_group']['#options'] = drupal_map_assoc(array_keys($groups['node'][$type]['teaser']));
	    	}
	    } else {
	    	$form['node_links_options_variables'][$type]['field_group']['#description'] = t('This setting requires Field group module.');
	    	$form['node_links_options_variables'][$type]['field_group']['#disabled'] = true;
	    }

	    $form['node_links_options_variables'][$type]['weight'] = array(
	        '#type' => 'select',
	        '#title' => t('Node links weight'),
	        '#default_value' => isset($settings[$type]['weight']) ? $settings[$type]['weight'] : '',
    		'#options' => drupal_map_assoc(range(-20, 20)),
    		'#empty_value' => '',
	        '#empty_option' => t('Default'),
	    );

	    $form['node_links_options_variables'][$type]['hide_links'] = array(
	        '#type' => 'checkbox',
	        '#title' => t('Hide node links'),
	        '#default_value' => isset($settings[$type]['hide_links']) ? $settings[$type]['hide_links'] : 0,
	    );

	    $form['node_links_options_variables'][$type]['readmore'] = array(
	        '#type' => 'fieldset',
	        '#title' => t('Read more link'),
	    );

	    $form['node_links_options_variables'][$type]['readmore']['class'] = array(
	        '#type' => 'textfield',
	        '#title' => t('Extra class'),
	        '#description' => t('Separate multiple classes by spaces'),
	        '#default_value' => isset($settings[$type]['readmore']['class']) ? $settings[$type]['readmore']['class'] : '',
	    );

	    $form['node_links_options_variables'][$type]['readmore']['text'] = array(
	        '#type' => 'textfield',
	        '#title' => t('Link text'),
	        '#default_value' => isset($settings[$type]['readmore']['text']) ? $settings[$type]['readmore']['text'] : '',
	    );

    }

    return system_settings_form($form);
}
